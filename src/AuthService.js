// src/AuthService.js
import { AuthServiceClient } from "./grpc/authentication_grpc_web_pb";
import { LoginRequest } from './grpc/authentication_pb';

const authService = new AuthServiceClient('http://localhost:8080',null, null);

export const login = async (username, password) => {
  const request = new LoginRequest();
  request.setUsername(username);
  request.setPassword(password);

  return new Promise( (resolve, reject) => {
    authService.login(request, {}, (err, response) => {
      if (err){
        reject(err);
      } else {
        resolve(response.getToken());
      }
    });
  } );
};
