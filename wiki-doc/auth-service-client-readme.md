
# Test gRPC AuthSerice

1. Install dependencies:

```shell
npm install --save-dev @babel/plugin-proposal-private-property-in-object

npm install grpc-web google-protobuf

```

2. make sure Envoy proxy is running. in this example it listen on port 8080 and forward to port 50051.

[Envoy proxy](../../03-proxy/README.md)

3. generate gRPC javascript

```shell
mkdir -p src/grpc # create directory `grpc` to store generated gPRC files.
cd ../../01-protobuf # change directory to proto file
protoc --js_out=import_style=commonjs:. --grpc-web_out=import_style=commonjs,mode=grpcwebtext:. authentication.proto
mv *.js ../04-front-end/fe-01-main-front-end/src/grpc/
```

4. implement AuthServiceClient

[src/AuthService.js](./src/AuthService.js)

5. implement login form and handler

[src/App.js](./src/App.js)

6. start React app and test from browser:

![auth-service-client-test-01.png](./images/auth-service-client-test-01.png)
